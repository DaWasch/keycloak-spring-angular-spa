#Stage 1 run mvn clean package

#Stage 2 Container Build
FROM adoptopenjdk/openjdk11:jre-11.0.7_10-alpine
COPY ./dist/frontend /app/frontend/
COPY ./target/*.jar /app/app.jar
RUN chown -R 1001:0 /app/* && chmod -R 755 /app/*
WORKDIR /app
CMD rm /app/frontend/assets/env.js
USER 1001
ENTRYPOINT ["java", "-jar", "app.jar"]
