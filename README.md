# Angular/Spring Boot/Keycloak/Docker Single Page Authentication Example

This example shows one possibility to implement an Angular frontend and Spring Boot Backend using Keycloak for authentication as a single page application.
Benefit of this is having frontend and backend in one container as Angular frontend is also using the Spring Boots Tomcat. So there is less overhead with deployment but also less flexibility in separating backend and frontend.

I also implemented the possibility to inject environment variables to backend and to frontend (via backend, look at SecurityConfig writeEnvFile). So definition of domain name, keycloak url... is possible on docker run (see docker-compose.yml, .env).

This repository is prepared for running with Let's Encrypt Proxy Companion (see docker-compose.yml network webproxy). Further Information: https://github.com/nginx-proxy/docker-letsencrypt-nginx-proxy-companion

I was using frontend maven plugin by Eirslett at first but commented it out for now (see pom.xml). Running Angular build as separate Build Stage in Jenkins (see Jenkinsfile) proved to be faster than in the two Maven stages package and test (because both are building the frontend). 
## Prerequisites
1. Keycloak up an running (See keycloak-docker-compose)
2. Configure Keycloak having a client called "fullstack-app", a client role "app-user" and a user owning this role
3. Configure Keycloak client "fullstack-app" and add following redirect urls
   1. http://localhost:4200/* (Angular ng serve url when run in IDE)
   2. http://localhost:8080/* (Spring Boot backend url when run in IDE)
   3. http://localhost:8980/* (Backend/Frontend url, see docker-compose-local.yml)
4. Configure web origins correspondingly 
   1. http://localhost:4200 (Angular ng serve url when run in IDE)
   2. http://localhost:8080 (Spring Boot backend url when run in IDE)
   3. http://localhost:8980 (Backend/Frontend url, see docker-compose-local.yml)
## Manual Build in IDE
1. checkout repository
2. make sure prerequisites are established
3. Install NodeJs, npm and Angular (Google)
4. build/run Angular (choose option)
   1. run "npm install && npm run build" to use Spring Boot web server and start whole application with IDE
   2. run "ng serve" to just run angular (http://localhost:4200)
5. run KeycloakSpringAngularSPAApplication as Java Application (or "mvn spring-boot:run -P local")
6. open http://localhost:8080 in Browser (if 4i) or http://localhost:4200 (if 4ii)
## Jenkins Build
1. make sure you have Jenkins with Docker support installed
2. Jenkinsfile is designed for Multibranch Pipeline but can also be modified for as single pipeline project adding git checkout stage (Google)
