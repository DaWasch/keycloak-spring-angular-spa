export const environment = {
  production: true,
  apiUrl: window['env']['apiUrl'] || 'default',
  authUrl: window['env']['authUrl'] || 'default',
  authRealm: window['env']['authRealm'] || 'default',
  authApplication: window['env']['authApplication'] || 'default'
};
