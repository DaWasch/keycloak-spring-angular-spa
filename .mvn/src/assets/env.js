(function(window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["apiUrl"] = "http://localhost:8080";
  window["env"]["authUrl"] = "http://localhost:8981/auth";
  window["env"]["authRealm"] = "master";
  window["env"]["authApplication"] = "fullstack-app";
})(this);
