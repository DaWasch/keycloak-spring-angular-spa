﻿import { KeycloakService } from 'keycloak-angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from './_models';
import { KeycloakProfile } from 'keycloak-js';
import { MatToolbar } from '@angular/material/toolbar';
import { MatButton} from '@angular/material/button'

@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent implements OnInit{
    userDetails: KeycloakProfile;
    isLoggedIn: boolean = false;
    constructor(private keycloakService: KeycloakService) {}

    async ngOnInit() {
      if (await this.keycloakService.isLoggedIn()) {
        this.userDetails = await this.keycloakService.loadUserProfile();
        this.isLoggedIn = true;
      }
    }

    async doLogout() {
      await this.keycloakService.logout();
      this.isLoggedIn = false;
    }
    async doLogin(){
      await this.keycloakService.login();
      this.isLoggedIn = true;
    }
}
