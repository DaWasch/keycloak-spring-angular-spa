import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '@environments/environment';
import { HelloMessage } from '../_models/helloMessage';

@Injectable({ providedIn: 'root' })
export class HelloService {
    constructor(private http: HttpClient) {
    }

    get() {
        return this.http.get<HelloMessage>(environment.apiUrl + '/api/hello');
    }
}
