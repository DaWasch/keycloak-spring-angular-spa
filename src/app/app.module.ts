﻿import { APP_INITIALIZER,NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { environment } from '../environments/environment';;
import { IndexComponent } from './index/index.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button'
import { MatCard, MatCardModule } from '@angular/material/card'
// tslint:disable-next-line: typedef
function initializeKeycloak(keycloak: KeycloakService): () => Promise<any>  {
  return (): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      const keycloakConfig = {
        url: environment.authUrl,
        realm: environment.authRealm,
        clientId: environment.authApplication,
        'ssl-required': 'external',
        'public-client': true
      };
      try {
        await keycloak.init({
          config: keycloakConfig,
          loadUserProfileAtStartUp: true,
          initOptions: {
            onLoad: 'check-sso',
            silentCheckSsoRedirectUri:
              window.location.origin + '/assets/silent-check-sso.html',
          },
          bearerExcludedUrls: []
        });
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  };
}

@NgModule({
  imports: [
      BrowserModule,
      ReactiveFormsModule,
      HttpClientModule,
      AppRoutingModule,
      KeycloakAngularModule,
      MatToolbarModule,
      BrowserAnimationsModule,
      MatIconModule,
    MatButtonModule,
      MatCardModule
  ],
    declarations: [
        AppComponent,
        HomeComponent
,
        IndexComponent    ],
    providers: [
        {
          provide: APP_INITIALIZER,
          useFactory: initializeKeycloak,
          multi: true,
          deps: [KeycloakService],
        },

    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
