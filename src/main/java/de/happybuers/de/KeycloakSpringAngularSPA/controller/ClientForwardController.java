package de.happybuers.de.KeycloakSpringAngularSPA.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
//@RolesAllowed("app-user")
public class ClientForwardController {
  @GetMapping(value = "/{path:[^\\\\.]*}")
  public String forwardHello() {
    return "forward:/index.html";
  }
  @GetMapping(value = "/")
  public String forwardRoot() {
    return "redirect:/index.html";
  }
}
