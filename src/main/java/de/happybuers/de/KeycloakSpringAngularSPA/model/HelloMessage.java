package de.happybuers.de.KeycloakSpringAngularSPA.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class HelloMessage {
    String message;
}
