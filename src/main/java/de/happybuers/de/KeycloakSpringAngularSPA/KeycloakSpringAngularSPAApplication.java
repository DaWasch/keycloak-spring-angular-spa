package de.happybuers.de.KeycloakSpringAngularSPA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeycloakSpringAngularSPAApplication {

	public static void main(String[] args) {
		SpringApplication.run(KeycloakSpringAngularSPAApplication.class, args);
	}


}
