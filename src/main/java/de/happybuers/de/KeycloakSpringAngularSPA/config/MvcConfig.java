//package de.happybuers.de.KeycloakSpringAngularSAP.config;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//@Configuration
//public class MvcConfig implements WebMvcConfigurer {
//  @Override
//  public void addResourceHandlers(ResourceHandlerRegistry registry) {
//    registry
//      .addResourceHandler("/resources/static/**")
//      .addResourceLocations("file:///C:/Users/daniel/Documents/GIT/keycloak-sap/dist/frontend");
//  }
//}
