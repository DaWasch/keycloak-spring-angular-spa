package de.happybuers.de.KeycloakSpringAngularSPA.config;

import lombok.extern.slf4j.Slf4j;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.keycloak.adapters.springsecurity.management.HttpSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Slf4j
@KeycloakConfiguration
public class SecurityConfig extends KeycloakWebSecurityConfigurerAdapter {

    @Value("${keycloak.auth-server-url}")
    private String keycloakUrl;
    @Value("${keycloak.realm}")
    private String keycloakRealm;
    @Value("${keycloak.resource}")
    private String keycloakResource;
    @Value("${environment.api-url}")
    private String apiUrl;
    @Value("${environment.frontend-url}")
    private String frontendUrl;

    @Value("${spring.resources.static-locations}")
    private String staticLocation;

    @Bean
    public KeycloakSpringBootConfigResolver keycloakConfigResolver() {
        return new KeycloakSpringBootConfigResolver();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) {
        SimpleAuthorityMapper grantedAuthorityMapper = new SimpleAuthorityMapper();
        grantedAuthorityMapper.setPrefix("ROLE_");
        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(grantedAuthorityMapper);
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

    @Bean
    @Override
    @ConditionalOnMissingBean(HttpSessionManager.class)
    protected HttpSessionManager httpSessionManager() {
        return new HttpSessionManager();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http
                .cors().and()
                .csrf().disable()
                .headers().frameOptions().sameOrigin().and()
                .authorizeRequests()
                .antMatchers("/sso/login").permitAll()
                .antMatchers("/").permitAll()
                .antMatchers("/actuator/health").permitAll()
                .antMatchers("/index.html").permitAll()
                .antMatchers("/favicon.*").permitAll()
                .antMatchers("/assets/**").permitAll()
                .antMatchers("/styles*").permitAll()
                .antMatchers("/*.js").permitAll()
                .antMatchers("/**").authenticated()
                .anyRequest().authenticated();
    }

  @Bean
  void writeEnvFile() throws IOException {
    String path = staticLocation.replace("file:","");
    if(!path.startsWith("/")){
      path = "./" + path;
    }
    File file = new File(path + "/assets/env.js");
    String str = "(function(window) {\n" +
      "  window[\"env\"] = window[\"env\"] || {};\n" +
      "\n" +
      "  // Environment variables\n" +
      "  window[\"env\"][\"apiUrl\"] = \"" + this.apiUrl+ "\";\n" +
      "  window[\"env\"][\"authUrl\"] = \""+ this.keycloakUrl + "\";\n" +
      "  window[\"env\"][\"authRealm\"] = \"" + this.keycloakRealm + "\";\n" +
      "  window[\"env\"][\"authApplication\"] = \"" + this.keycloakResource + "\";\n" +
      "})(this);";
    log.info("write env.js: " + str);
    FileOutputStream outputStream = new FileOutputStream(file);
    byte[] str2Bytes = str.getBytes(StandardCharsets.UTF_8);
    outputStream.write(str2Bytes);
    outputStream.close();
  }

}
